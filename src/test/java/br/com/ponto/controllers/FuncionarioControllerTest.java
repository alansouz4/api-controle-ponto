package br.com.ponto.controllers;

import br.com.ponto.models.Funcionario;
import br.com.ponto.services.FuncionarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;

@WebMvcTest(FuncionarioController.class)
public class FuncionarioControllerTest {

    @MockBean
    private FuncionarioService funcionarioService;

    @Autowired
    private MockMvc mockMvc;

    Funcionario funcionario;

    @BeforeEach
    public void setUp() {
        funcionario = new Funcionario();
        funcionario.setId(1);
        funcionario.setNomeCompleto("Alan Augusto");
        funcionario.setCpf("667.210.870-06");
        funcionario.setEmail("lan_augusto@yahoo.com.br");
        funcionario.setDataCadastro(LocalDate.now());
    }

    @Test
    public void testarRetornarSucesso_QuandoCriarFuncionario() throws Exception {
        funcionario.setDataCadastro(null);
        funcionario.setId(1);

        ObjectMapper mapper = new ObjectMapper();
        String funcionarioJson = mapper.writeValueAsString(funcionario);


        Mockito.when(funcionarioService.saveFuncionario(Mockito.any(Funcionario.class))).thenReturn(funcionario);
        mockMvc.perform(MockMvcRequestBuilders.post("/funcionarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(funcionarioJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarRetornarSucesso_QuandoListarFuncionarios() throws Exception {
        Iterable<Funcionario> funcionarios = new ArrayList<>();

        ObjectMapper mapper = new ObjectMapper();
        String funcionarioJson = mapper.writeValueAsString(funcionarios);

        Mockito.when(funcionarioService.getFuncionario()).thenReturn(funcionarios);
        mockMvc.perform(MockMvcRequestBuilders.get("/funcionarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(funcionarioJson))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void testarRetornarSucesso_QuandoListarFuncionarioPorId() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String funcionarioJson = mapper.writeValueAsString(funcionario);

        Mockito.when(funcionarioService.getFuncionarioById(1)).thenReturn(funcionario);
        mockMvc.perform(MockMvcRequestBuilders.get("/funcionarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(funcionarioJson))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
}
