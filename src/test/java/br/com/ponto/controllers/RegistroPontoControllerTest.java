package br.com.ponto.controllers;

import br.com.ponto.enums.TipoRegistroEnum;
import br.com.ponto.models.Funcionario;
import br.com.ponto.models.RegistroPonto;
import br.com.ponto.models.dtos.RegistroPontoSaidaDTO;
import br.com.ponto.services.RegistroPontoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(RegistroPontoController.class)
public class RegistroPontoControllerTest {

    @MockBean
    private RegistroPontoService registroPontoService;

    @Autowired
    private MockMvc mockMvc;

    RegistroPonto registroPontoEntrada;
    RegistroPonto registroPontoSaida;
    Funcionario funcionario;

    @BeforeEach
    public void setUp() {
        funcionario = new Funcionario();
        funcionario.setId(1);

        registroPontoEntrada = new RegistroPonto();
        registroPontoEntrada.setId(1);
        registroPontoEntrada.setFuncionario(funcionario);
        registroPontoEntrada.setTipoRegistro(TipoRegistroEnum.ENTRADA);
        registroPontoEntrada.setDataHoraRegistro(LocalDateTime.now());

        registroPontoSaida = new RegistroPonto();
        registroPontoSaida.setId(1);
        registroPontoSaida.setFuncionario(funcionario);
        registroPontoSaida.setTipoRegistro(TipoRegistroEnum.SAIDA);
        registroPontoSaida.setDataHoraRegistro(LocalDateTime.now());

    }

    @Test
    public void testarRetornarSucesso_QuandoCriarRegistroPonto() throws Exception {
        Mockito.when(registroPontoService.saveRegistro(Mockito.any(RegistroPonto.class))).thenReturn(registroPontoEntrada);
        registroPontoEntrada.setDataHoraRegistro(null);

        ObjectMapper mapper = new ObjectMapper();
        String registroJson = mapper.writeValueAsString(registroPontoEntrada);

        mockMvc.perform(MockMvcRequestBuilders.post("/registros")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registroJson))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarRetornarSucesso_QuandoListarRegistrosPonto() throws Exception {

        Iterable<RegistroPonto> registrosPonto = Arrays.asList(registroPontoEntrada,registroPontoSaida);
        Mockito.when(registroPontoService.getRegistro()).thenReturn(registrosPonto);

        ObjectMapper mapper = new ObjectMapper();
        String registroJson = mapper.writeValueAsString(registrosPonto);

        mockMvc.perform(MockMvcRequestBuilders.get("/registros")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registroJson))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void testarRetornarSucesso_QuandoListarRegistroPontoPorId() throws Exception {

        Iterable<RegistroPonto> registrosPonto = Arrays.asList(registroPontoEntrada, registroPontoSaida);

        RegistroPontoSaidaDTO registroPontoSaidaDTO = new RegistroPontoSaidaDTO();

        registroPontoSaidaDTO.setTotalHorasTrabalhadas(BigDecimal.valueOf(8.00)
                .setScale(2, RoundingMode.HALF_UP));

        registroPontoSaidaDTO.setPontosFuncionario(registrosPonto);

        Mockito.when(registroPontoService.getRegistroByIdFuncionario(Mockito.anyInt())).thenReturn(registroPontoSaidaDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/registros/1"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
