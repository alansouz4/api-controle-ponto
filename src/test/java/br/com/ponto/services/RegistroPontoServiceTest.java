package br.com.ponto.services;

import br.com.ponto.enums.TipoRegistroEnum;
import br.com.ponto.models.Funcionario;
import br.com.ponto.models.RegistroPonto;
import br.com.ponto.models.dtos.RegistroPontoSaidaDTO;
import br.com.ponto.repositories.RegistroPontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class RegistroPontoServiceTest {

    @MockBean
    private RegistroPontoRepository registroPontoRepository;

    @MockBean
    private FuncionarioService funcionarioService;

    @Autowired
    private RegistroPontoService registroPontoService;

    RegistroPonto registroPontoEntrada;
    RegistroPonto registroPontoSaida;
    Funcionario funcionario;
    Funcionario funcionarioTeste;

    @BeforeEach
    public void setUp() {
        funcionario = new Funcionario();
        funcionario.setId(1);
        funcionario.setNomeCompleto("Alan Augusto");
        funcionario.setCpf("38634454819");
        funcionario.setEmail("lan_augusto@yahoo.com.br");
        funcionario.setDataCadastro(LocalDate.now());

        funcionarioTeste = new Funcionario();
        funcionarioTeste.setId(2);

        registroPontoEntrada = new RegistroPonto();
        registroPontoEntrada.setId(1);
        registroPontoEntrada.setFuncionario(funcionario);
        registroPontoEntrada.setTipoRegistro(TipoRegistroEnum.ENTRADA);
        registroPontoEntrada.setDataHoraRegistro(LocalDateTime.parse("2020-07-05T02:00:00"));

        registroPontoSaida = new RegistroPonto();
        registroPontoSaida.setId(1);
        registroPontoSaida.setFuncionario(funcionario);
        registroPontoSaida.setTipoRegistro(TipoRegistroEnum.SAIDA);
        registroPontoSaida.setDataHoraRegistro(LocalDateTime.parse("2020-07-05T10:30:00"));

        Mockito.when(funcionarioService.getFuncionarioById(Mockito.anyInt())).thenReturn(funcionario);
    }

    @Test
    public void testarRetornoSucesso_QuandoCriarRegistroPonto() {
        Mockito.when(funcionarioService.getFuncionarioById(Mockito.anyInt())).thenReturn(funcionario);

        Mockito.when(registroPontoRepository.save(registroPontoEntrada)).then(leadLamb -> {
            registroPontoEntrada.setId(1);
            return registroPontoEntrada;
        });

        RegistroPonto registroPontoObjeto = registroPontoService.saveRegistro(registroPontoEntrada);

        Assertions.assertEquals(1, registroPontoObjeto.getId());
    }

    @Test
    public void testarRetornoSuccesso_QuandoListarRegistrosPonto(){
        Iterable<RegistroPonto> registrosPonto = Arrays.asList(registroPontoEntrada);
        Mockito.when(registroPontoRepository.findAll()).thenReturn(registrosPonto);
        Iterable<RegistroPonto> registrosPontoObjeto = registroPontoService.getRegistro();
        Assertions.assertSame(registrosPonto,registrosPontoObjeto);
    }

    @Test
    public void testarRetornoSucesso_QuandoListarRegistroPontoPorId(){
        Iterable<RegistroPonto> registrosPonto = Arrays.asList(registroPontoEntrada, registroPontoSaida);
        Mockito.when(registroPontoRepository.findAllByFuncionario(Mockito.any(Funcionario.class))).thenReturn(registrosPonto);

        RegistroPontoSaidaDTO registroPontoDTOId = registroPontoService.getRegistroByIdFuncionario(2);

        Assertions.assertEquals(registroPontoDTOId.getTotalHorasTrabalhadas(), BigDecimal.valueOf(8.50)
                .setScale(2, RoundingMode.HALF_UP));
    }
}
