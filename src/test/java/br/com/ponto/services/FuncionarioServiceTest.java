package br.com.ponto.services;

import br.com.ponto.models.Funcionario;
import br.com.ponto.repositories.FuncionarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class FuncionarioServiceTest {

    @MockBean
    private FuncionarioRepository funcionarioRepository;

    @Autowired
    private FuncionarioService funcionarioService;

    Funcionario funcionario;

    @BeforeEach
    public void setUp() {
        funcionario = new Funcionario();
        funcionario.setId(1);
        funcionario.setNomeCompleto("Alan Augusto");
        funcionario.setCpf("667.210.870-06");
        funcionario.setEmail("lan_augusto@yahoo.com.br");
        funcionario.setDataCadastro(LocalDate.now());
    }

    @Test
    public void testarRetornoSucesso_QuandoCriarFuncionario() {
        Mockito.when(funcionarioRepository.save(funcionario)).thenReturn(funcionario);
        Funcionario funcionarioObjeto = funcionarioService.saveFuncionario(funcionario);
        Assertions.assertSame(funcionarioObjeto, funcionario);
    }

    @Test
    public void testarRetornoNull_QuandoCriarFuncionario() {
        Mockito.when(funcionarioRepository.save(null)).thenReturn(funcionario);
        Funcionario funcionarioObjeto = funcionarioService.saveFuncionario(funcionario);
        Assertions.assertSame(null, funcionarioObjeto);
    }

    @Test
    public void testarRetornoSuccesso_QuandoListarFuncionarios(){
        Iterable<Funcionario> funcionarios = Arrays.asList(funcionario);
        Mockito.when(funcionarioRepository.findAll()).thenReturn(funcionarios);
        Iterable<Funcionario> funcionariosObjeto = funcionarioService.getFuncionario();
        Assertions.assertSame(funcionarios,funcionariosObjeto);
    }

    @Test
    public void testarRetornoNull_QuandoListarFuncionarios(){
        Iterable<Funcionario> funcionarios = Arrays.asList(funcionario);
        Mockito.when(funcionarioRepository.findAll()).thenReturn(null);
        Iterable<Funcionario> funcionariosObjeto = funcionarioService.getFuncionario();
        Assertions.assertSame(null,funcionariosObjeto);
    }

    @Test
    public void testarRetornoSucesso_QuandoListarFuncionarioPorId(){
        Mockito.when(funcionarioRepository.findById(1)).thenReturn(Optional.of(funcionario));

        Funcionario funcionarioId = funcionarioService.getFuncionarioById(1);

        Assertions.assertEquals(funcionario, funcionarioId);
    }

}
