package br.com.ponto.controllers;

import br.com.ponto.models.Funcionario;
import br.com.ponto.services.FuncionarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/funcionarios")
public class FuncionarioController {

    @Autowired
    private FuncionarioService funcionarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Funcionario saveFuncionario(@RequestBody @Valid Funcionario funcionario){
        Funcionario funcionarioObjeto = funcionarioService.saveFuncionario(funcionario);
        return funcionarioObjeto;
    }

    @GetMapping
    public Iterable<Funcionario> getFuncionarios(){
        Iterable<Funcionario> usuarios = funcionarioService.getFuncionario();
        return usuarios;
    }

    @GetMapping("/{id}")
    public Funcionario getFuncionarioById(@PathVariable(name = "id") int id){
        try{
            return funcionarioService.getFuncionarioById(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Funcionario> updateFuncionario(@PathVariable(name = "id") @Valid int id,
                                                         @RequestBody Funcionario funcionario){
        try{
            Funcionario funcionarioObjeto = funcionarioService.updateFuncionario(id, funcionario);
            return ResponseEntity.status(200).body(funcionarioObjeto);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
