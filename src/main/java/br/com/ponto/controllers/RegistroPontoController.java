package br.com.ponto.controllers;

import br.com.ponto.models.RegistroPonto;
import br.com.ponto.models.dtos.RegistroPontoSaidaDTO;
import br.com.ponto.services.RegistroPontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/registros")
public class RegistroPontoController {

    @Autowired
    private RegistroPontoService registroPontoService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RegistroPonto saveBatidaPonto(@RequestBody @Valid RegistroPonto registro){
        return registroPontoService.saveRegistro(registro);
    }

    @GetMapping
    public Iterable<RegistroPonto> getRegistros(){
        return registroPontoService.getRegistro();
    }

//    @GetMapping
//    public ModelAndView getRegistros(){
//        ModelAndView mv = new ModelAndView("registros");
//        Iterable<RegistroPonto> registros = registroPontoService.getRegistro();
//        mv.addObject("registros", registros);
//        return mv;
//    }

    @GetMapping("/{id}")
    public RegistroPontoSaidaDTO getRegistroById(@PathVariable(name = "id") int id){
        try{
            RegistroPontoSaidaDTO registroPontoDTO = registroPontoService.getRegistroByIdFuncionario(id);
            return  registroPontoDTO;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }


    }
}
