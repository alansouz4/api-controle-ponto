package br.com.ponto.models.dtos;

import br.com.ponto.models.RegistroPonto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegistroPontoSaidaDTO {

    Iterable<RegistroPonto> pontosFuncionario;
    BigDecimal totalHorasTrabalhadas;
}
