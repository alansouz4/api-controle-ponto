package br.com.ponto.models;

import br.com.ponto.enums.TipoRegistroEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistroPonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    private Funcionario funcionario;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Campo tipo registro é obrigatório")
    private TipoRegistroEnum tipoRegistro;

    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime dataHoraRegistro;
}
