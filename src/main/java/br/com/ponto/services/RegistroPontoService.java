package br.com.ponto.services;

import br.com.ponto.enums.TipoRegistroEnum;
import br.com.ponto.models.Funcionario;
import br.com.ponto.models.RegistroPonto;
import br.com.ponto.models.dtos.RegistroPontoSaidaDTO;
import br.com.ponto.repositories.RegistroPontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;

@Service
public class RegistroPontoService {

    @Autowired
    private RegistroPontoRepository registroPontoRepository;

    @Autowired
    private FuncionarioService funcionarioService;

    public RegistroPonto saveRegistro(RegistroPonto registro) {

        Funcionario funcionario = funcionarioService.getFuncionarioById(registro.getFuncionario().getId());
        if(funcionario != null){
            registro.setFuncionario(funcionario);
        }else{
            throw new RuntimeException("Usuário não encontrado");
        }

        registro.setDataHoraRegistro(LocalDateTime.now());

        RegistroPonto registroPonto = registroPontoRepository.save(registro);

        return registroPonto;
    }


    public Iterable<RegistroPonto> getRegistro() {
        return registroPontoRepository.findAll();
    }


    public RegistroPontoSaidaDTO getRegistroByIdFuncionario(int id){
        LocalDateTime entrada = null;
        LocalDateTime saida = null;
        Long quantidadeHoras = (long) 0;
        RegistroPontoSaidaDTO pontoSaidaDTO = new RegistroPontoSaidaDTO();

        Funcionario funcionario = funcionarioService.getFuncionarioById(id);
        Iterable<RegistroPonto> pontosFuncionario = registroPontoRepository.findAllByFuncionario(funcionario);

        for (RegistroPonto ponto : pontosFuncionario) {
            if (ponto.getTipoRegistro() == TipoRegistroEnum.ENTRADA) {
                entrada = ponto.getDataHoraRegistro();
            } else {
                saida = ponto.getDataHoraRegistro();
            }

            if (entrada != null && saida != null) {
                {
                    Duration horasTrabalhadas = Duration.between(entrada, saida);
                    quantidadeHoras += horasTrabalhadas.toMinutes();
                    entrada = null;
                    saida = null;
                }
            }
        }
            pontoSaidaDTO.setPontosFuncionario(pontosFuncionario);
            pontoSaidaDTO.setTotalHorasTrabalhadas(new BigDecimal((double) quantidadeHoras / 60)
                    .setScale(2, RoundingMode.HALF_UP));
            return pontoSaidaDTO;
    }

}
