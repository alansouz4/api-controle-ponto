package br.com.ponto.services;

import br.com.ponto.models.Funcionario;
import br.com.ponto.repositories.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class FuncionarioService {

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    public Funcionario saveFuncionario(Funcionario funcionario) {
        funcionario.setDataCadastro(LocalDate.now());
        return funcionarioRepository.save(funcionario);
    }

    public Iterable<Funcionario> getFuncionario() {
        return funcionarioRepository.findAll();
    }

    public Funcionario getFuncionarioById(int id) {
        Optional<Funcionario> funcionarioOptional = funcionarioRepository.findById(id);
        if(funcionarioOptional.isPresent()){
            return funcionarioOptional.get();
        } else{
            throw new RuntimeException("Usuário não encontrado");
        }
    }

    public Funcionario updateFuncionario(int id, Funcionario funcionario) {
        if(funcionarioRepository.existsById(id)){
            Funcionario funcionarioObjeto = saveFuncionario(funcionario);
            return funcionarioObjeto;
        } else{
            throw new RuntimeException("Usuário não encontrado!");
        }
    }
}
