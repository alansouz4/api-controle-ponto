package br.com.ponto.services;

import br.com.ponto.models.Funcionario;
import br.com.ponto.repositories.FuncionarioRepository;
import br.com.ponto.security.LoginFuncionario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LoginFuncionarioService implements UserDetailsService {

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Funcionario funcionario = funcionarioRepository.findByEmail(email);
        if ((funcionario.equals(null))){
            throw new UsernameNotFoundException(email);
        }

        LoginFuncionario loginFuncionario = new LoginFuncionario(funcionario.getId(), funcionario.getEmail(), funcionario.getSenha());
        return loginFuncionario;
    }
}
