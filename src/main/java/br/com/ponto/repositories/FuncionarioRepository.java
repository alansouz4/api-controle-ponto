package br.com.ponto.repositories;

import br.com.ponto.models.Funcionario;
import org.springframework.data.repository.CrudRepository;

public interface FuncionarioRepository extends CrudRepository<Funcionario, Integer> {

    Funcionario findByEmail(String email);
}
