package br.com.ponto.repositories;

import br.com.ponto.models.Funcionario;
import br.com.ponto.models.RegistroPonto;
import org.springframework.data.repository.CrudRepository;

public interface RegistroPontoRepository extends CrudRepository<RegistroPonto, Integer> {
    Iterable<RegistroPonto> findAllByFuncionario(Funcionario funcionario);
}
