# *API Ponto Eletrônico*


### - Executar git clone https://gitlab.com/alansouz4/api-controle-ponto.

### - Ajustar credenciais para conexão com o banco de dados no arquivo application.properties.

### - Executar o comando mvn spring-boot:run dentro da pasta raiz do projeto para executar o sistema.

---------

# *JSON*

## Funcionário

### POST /funcionarios

Cadastra um novo funcionario no sistema.

- @RequestBody

```{
    "nomeCompleto": "João Moreira",
    "cpf": "0000000000",
    "email": "joaon@gmail.com"
}

- Response 201

{
    "id": 1,
    "nomeCompleto": "João Moreira",
    "cpf": "0000000000",
    "email": "joaon@gmail.com",
    "dataCadastro": "2020-06-29"
}
```

### GET /funcionarios

Exibe a lista de funcionarios no sistema.

- Response 200

```
[
    {
        "id": 1,
        "nomeCompleto": "João Moreira",
        "cpf": "0000000000",
        "email": "joaon@gmail.com",
        "dataCadastro": "2020-06-29"
    },
    {
        "id": 2,
        "nomeCompleto": "Maria Moreira",
        "cpf": "11111111111",
        "email": "maria@yahoo.com.br",
        "dataCadastro": "2020-06-29"
    }
]
```

### PUT /{id}

Atualiza os dados de um funcionario.


- @PathVariable
- @RequestBody

```
{
    "id": 3,
    "nomeCompleto": "Geovanna Moreira",
    "cpf": "9999999999",
    "email": "geo.moreira@yahoo.com.br"
}

- Response 200

{
    "idUsuario": 3,
    "nomeCompleto": "Geovanna Moreira",
    "cpf": "9999999999",
    "email": "geo.moreira@yahoo.com.br",
    "dataCadastro": "2020-06-30"
}
```

- Response 400


## Registros Ponto

### POST /registros

- @RequestBody

```
{
    "tipoRegistro": "ENTRADA",
    "funcionario": {
        "id": 2
    }
}
```

- Response 201

```
{
    "id": 1,
    "funcionario": {
        "id": 2,
        "nomeCompleto": "Maria Moreira",
        "cpf": "111111111111111",
        "email": "maria@yahoo.com.br",
        "dataCadastro": "2020-06-29 00:00:00"
    },
    "tipoRegistro": "ENTRADA",
    "dataHoraRegistro": "2020-06-30 22:45:29"
}
```

### GET /registros

Exibe a lista de registros no sistema.

- Response 200

```
[
    {
        "id": 1,
        "funcionario": {
            "id": 2,
            "nomeCompleto": "Maria Moreira",
            "cpf": "111111111111111",
            "email": "maria@yahoo.com.br",
            "dataCadastro": "2020-06-29 00:00:00"
        },
        "tipoRegistro": "ENTRADA",
        "dataHoraRegistro": "2020-06-30 22:45:29"
    },
    {
        "id": 2,
        "funcionario": {
            "id": 2,
            "nomeCompleto": "Maria Moreira",
            "cpf": "111111111111111",
            "email": "maria@yahoo.com.br",
            "dataCadastro": "2020-06-29 00:00:00"
        },
        "tipoRegistro": "SAIDA",
        "dataHoraRegistro": "2020-06-30 22:45:29"
    }
]
```

## FRONT-END

### GET/registros

Exibe a lista de registros

- Response 200

![](Captura_de_tela_de_2020-07-05_20-05-03.png)
------------------------------------------------




 > :bust_in_silhouette: [LinkedIn Author](https://www.linkedin.com/in/alan-augusto-/)
